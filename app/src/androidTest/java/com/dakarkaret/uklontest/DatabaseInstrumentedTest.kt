package com.dakarkaret.uklontest

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.dakarkaret.uklontest.repository.database.AppDatabase
import com.dakarkaret.uklontest.repository.database.PostDao
import com.dakarkaret.uklontest.repository.entities.Post
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DatabaseInstrumentedTest {
    private var postDao: PostDao? = null
    private var mainDatabase: AppDatabase? = null

    @Before
    fun createDatabase() {
        mainDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getTargetContext(),
                AppDatabase::class.java).build()
        postDao = mainDatabase?.postDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        mainDatabase?.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertPost() {
        postDao?.insertAll(listOf(
                Post(1, 2, "title_text", "body_text"),
                Post(2, 3, "title_text", "body_text")))
        assertEquals(postDao?.getPosts()?.size, 2)
    }
}