package com.dakarkaret.uklontest.repository.network

import com.dakarkaret.uklontest.app.App
import com.dakarkaret.uklontest.repository.entities.Comment
import com.dakarkaret.uklontest.repository.entities.Post
import com.dakarkaret.uklontest.repository.network.WebService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class WebRepo {
    @Inject
    lateinit var webService: WebService

    init {
        App.appComponent.inject(this)
    }

    fun getPosts(): Observable<ArrayList<Post>> = webService.getWebApi()
                .getPosts()
                .subscribeOn(Schedulers.io())

    fun getComments(id: Int): Observable<ArrayList<Comment>> = webService.getWebApi()
                .getComments(id)
                .subscribeOn(Schedulers.io())
}