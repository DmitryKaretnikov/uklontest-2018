package com.dakarkaret.uklontest.repository.network

import com.dakarkaret.uklontest.repository.entities.Comment
import com.dakarkaret.uklontest.repository.entities.Post
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.*

interface WebApi {
    @GET("/posts")
    fun getPosts(): Observable<ArrayList<Post>>

    @GET("/posts/{id}/comments")
    fun getComments(@Path("id") id: Int): Observable<ArrayList<Comment>>
}