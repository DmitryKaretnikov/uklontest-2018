package com.dakarkaret.uklontest.repository.database

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.dakarkaret.uklontest.repository.entities.Comment
import com.dakarkaret.uklontest.repository.entities.Post

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertAll(list: List<T>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(item: T)

    @Delete
    fun deleteAll(list: List<T>)

    @Delete
    fun delete(item: T)
}

@Dao
abstract class PostDao : BaseDao<Post> {
    @Query("SELECT * FROM posts")
    abstract fun getPosts(): List<Post>

    @Query("SELECT * FROM posts")
    abstract fun getPostsDataSource(): DataSource.Factory<Int, Post>

    @Query("SELECT * FROM posts  WHERE id IN(:postId)")
    abstract fun getPost(postId: Int): Post

    @Query("DELETE FROM posts")
    abstract fun clearPosts()
}

@Dao
abstract class CommentDao : BaseDao<Comment> {
    @Query("SELECT * FROM comments WHERE postId IN(:postId)")
    abstract fun getCommentsLiveByPostId(postId: Int): LiveData<List<Comment>>

    @Query("DELETE FROM comments WHERE postId IN(:postId)")
    abstract fun clearCommentWithPostId(postId: Int)
}