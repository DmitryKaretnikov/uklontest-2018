package com.dakarkaret.uklontest.repository.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.dakarkaret.uklontest.repository.entities.Comment
import com.dakarkaret.uklontest.repository.entities.Post

@Database(entities = [Post::class, Comment::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao

    abstract fun commentDao(): CommentDao
}