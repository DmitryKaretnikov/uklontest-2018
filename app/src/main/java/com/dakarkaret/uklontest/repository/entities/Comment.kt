package com.dakarkaret.uklontest.repository.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "comments")
data class Comment(@PrimaryKey(autoGenerate = true) val id: Int,
                   val postId: Int,
                   val name: String,
                   val email: String,
                   val body: String)