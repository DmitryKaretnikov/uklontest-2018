package com.dakarkaret.uklontest.repository.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.v7.util.DiffUtil

@Entity(tableName = "posts")
data class Post(@PrimaryKey val id: Int,
                val userId: Int,
                val title: String,
                val body: String) {

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other?.javaClass != javaClass)
            return false

        val user = other as Post
        return id == user.id && body == user.body
    }
}