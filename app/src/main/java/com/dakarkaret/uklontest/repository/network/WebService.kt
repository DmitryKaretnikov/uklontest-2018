package com.dakarkaret.uklontest.repository.network

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://jsonplaceholder.typicode.com"

class WebService {
    private var webApi: WebApi? = null

    fun getWebApi(): WebApi = webApi ?: createWebApi()!!

    private fun createWebApi(): WebApi? {
        webApi = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(WebApi::class.java)
        return webApi
    }
}