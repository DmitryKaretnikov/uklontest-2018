package com.dakarkaret.uklontest.feature.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.Disposable
import java.util.ArrayList

abstract class BaseViewModel : ViewModel(), LifecycleObserver {
    protected val disposables = ArrayList<Disposable>()

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected fun destroy() {
        for (disposable in disposables) {
            disposable.dispose()
        }
    }
}