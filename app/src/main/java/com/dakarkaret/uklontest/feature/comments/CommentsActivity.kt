package com.dakarkaret.uklontest.feature.comments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.Menu
import android.view.MenuItem
import com.dakarkaret.uklontest.R
import com.dakarkaret.uklontest.feature.base.BaseActivity
import kotlinx.android.synthetic.main.activity_comments.*
import kotlinx.android.synthetic.main.item_post.*

class CommentsActivity : BaseActivity() {
    private lateinit var commentsViewModel: CommentsViewModel
    private val commentsAdapter: CommentsAdapter = CommentsAdapter()

    companion object {
        const val INTENT_KEY_POST_ID = "POST_ID"

        fun getIntent(context: Context, postId: Int): Intent =
                Intent(context, CommentsActivity::class.java)
                        .putExtra(INTENT_KEY_POST_ID, postId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(commentsToolbar)
        commentsRecycler.adapter = commentsAdapter
        commentsRecycler.layoutManager = LinearLayoutManager(this)
        window.exitTransition = TransitionInflater.from(this).inflateTransition(R.transition.slide_right)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.loadAction -> {
                commentsViewModel.loadCommentsToDatabase()
                return true
            }
            R.id.clearAction -> {
                commentsViewModel.clearAllComments()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initViewModel() {
        commentsViewModel = ViewModelProviders.of(this).get(CommentsViewModel::class.java)
        lifecycle.addObserver(commentsViewModel)
        commentsViewModel.postId = intent.extras.getInt(INTENT_KEY_POST_ID)
        commentsViewModel.loadPost()
    }

    override fun initLiveData() {
        commentsViewModel.getCommentsLive().observe(this, Observer { commentsAdapter.setComments(it) })
        commentsViewModel.currentPost.observe(this, Observer {
            userIdText.text = it?.userId.toString()
            titleText.text = it?.title
            bodyText.text = it?.body
        })
    }

    override fun getLayoutId(): Int = R.layout.activity_comments
}