package com.dakarkaret.uklontest.feature.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initViewModel()
        initLiveData()
    }

    abstract fun initViewModel()

    abstract fun initLiveData()

    abstract fun getLayoutId(): Int
}