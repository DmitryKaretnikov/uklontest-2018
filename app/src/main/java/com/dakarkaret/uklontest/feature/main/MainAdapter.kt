package com.dakarkaret.uklontest.feature.main

import android.app.Activity
import android.arch.paging.PagedListAdapter
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dakarkaret.uklontest.R
import com.dakarkaret.uklontest.feature.comments.CommentsActivity
import com.dakarkaret.uklontest.repository.entities.Post
import kotlinx.android.synthetic.main.item_post.view.*


class MainAdapter : PagedListAdapter<Post, MainAdapter.PostHolder>(Post.DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder =
            PostHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_post, parent, false))

    override fun onBindViewHolder(holder: PostHolder, position: Int) = holder.bind(getItem(position))

    class PostHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(post: Post?) {
            post?.let {
                itemView.userIdText.text = it.userId.toString()
                itemView.titleText.text = it.title
                itemView.bodyText.text = it.body
                itemView.setOnClickListener { view ->
                    view.context.startActivity(CommentsActivity.getIntent(view.context, it.id),
                            ActivityOptionsCompat.makeSceneTransitionAnimation(itemView.context as Activity)
                                    .toBundle())
                }
            }
        }
    }
}