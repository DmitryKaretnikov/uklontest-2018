package com.dakarkaret.uklontest.feature.comments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.dakarkaret.uklontest.app.App
import com.dakarkaret.uklontest.feature.base.BaseViewModel
import com.dakarkaret.uklontest.repository.network.WebRepo
import com.dakarkaret.uklontest.repository.database.CommentDao
import com.dakarkaret.uklontest.repository.database.PostDao
import com.dakarkaret.uklontest.repository.entities.Comment
import com.dakarkaret.uklontest.repository.entities.Post
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CommentsViewModel : BaseViewModel() {
    @Inject
    lateinit var webRepo: WebRepo
    @Inject
    lateinit var postDao: PostDao
    @Inject
    lateinit var commentDao: CommentDao
    var postId: Int = 0
    val currentPost: MutableLiveData<Post> = MutableLiveData()

    init {
        App.appComponent.inject(this)
    }

    fun loadCommentsToDatabase() {
        disposables.add(webRepo.getComments(postId)
                .subscribe({ commentDao.insertAll(it) }))
    }

    fun loadPost() {
        disposables.add(Observable.fromCallable { postDao.getPost(postId) }
                .subscribeOn(Schedulers.io())
                .subscribe({ currentPost.postValue(it) }))
    }

    fun clearAllComments() {
        disposables.add(Observable.fromCallable { commentDao.clearCommentWithPostId(postId) }
                .subscribeOn(Schedulers.io())
                .subscribe())
    }

    fun getCommentsLive(): LiveData<List<Comment>> = commentDao.getCommentsLiveByPostId(postId)
}