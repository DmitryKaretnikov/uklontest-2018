package com.dakarkaret.uklontest.feature.comments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dakarkaret.uklontest.R
import com.dakarkaret.uklontest.repository.entities.Comment
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentsHolder>() {
    private var comments: ArrayList<Comment> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsHolder =
            CommentsHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_comment, parent, false))

    override fun onBindViewHolder(holder: CommentsHolder, position: Int) = holder.bind(comments[position])

    override fun getItemCount(): Int = comments.size

    fun setComments(list: List<Comment>?) {
        comments = list as ArrayList<Comment>
        notifyDataSetChanged()
    }

    class CommentsHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(comment: Comment) {
            itemView.nameText.text = comment.name
            itemView.emailText.text = comment.email
            itemView.bodyText.text = comment.body
        }
    }
}