package com.dakarkaret.uklontest.feature.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.Menu
import android.view.MenuItem
import com.dakarkaret.uklontest.R
import com.dakarkaret.uklontest.feature.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private lateinit var mainViewModel: MainViewModel
    private val postsAdapter: MainAdapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(mainToolbar)
        mainRecycler.layoutManager = LinearLayoutManager(this)
        mainRecycler.adapter = postsAdapter
        window.exitTransition = TransitionInflater.from(this).inflateTransition(R.transition.slide_right)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.loadAction -> {
                mainViewModel.loadPostsToDatabase()
                return true
            }
            R.id.clearAction -> {
                mainViewModel.clearAllPosts()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        lifecycle.addObserver(mainViewModel)
    }

    override fun initLiveData() {
        mainViewModel.getPostsSource().observe(this, Observer { postsAdapter.submitList(it) })
    }

    override fun getLayoutId(): Int = R.layout.activity_main
}