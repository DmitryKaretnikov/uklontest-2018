package com.dakarkaret.uklontest.feature.main

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.dakarkaret.uklontest.app.App
import com.dakarkaret.uklontest.feature.base.BaseViewModel
import com.dakarkaret.uklontest.repository.network.WebRepo
import com.dakarkaret.uklontest.repository.database.PostDao
import com.dakarkaret.uklontest.repository.entities.Post
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel : BaseViewModel() {
    @Inject
    lateinit var webRepo: WebRepo
    @Inject
    lateinit var postDao: PostDao

    init {
        App.appComponent.inject(this)
    }

    fun loadPostsToDatabase() {
        disposables.add(webRepo.getPosts()
                .subscribe({ postDao.insertAll(it) }))
    }

    fun clearAllPosts() {
        disposables.add(Observable.fromCallable { postDao.clearPosts() }
                .subscribeOn(Schedulers.io())
                .subscribe())
    }

    fun getPostsSource(): LiveData<PagedList<Post>> {
        return LivePagedListBuilder(postDao.getPostsDataSource(), 10).build()
    }
}