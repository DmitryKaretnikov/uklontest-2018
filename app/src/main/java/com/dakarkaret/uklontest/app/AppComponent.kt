package com.dakarkaret.uklontest.app

import com.dakarkaret.uklontest.feature.comments.CommentsViewModel
import com.dakarkaret.uklontest.feature.main.MainViewModel
import com.dakarkaret.uklontest.repository.network.WebRepo
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(webRepo: WebRepo)
    fun inject(mainViewModel: MainViewModel)
    fun inject(commentsViewModel: CommentsViewModel)
}