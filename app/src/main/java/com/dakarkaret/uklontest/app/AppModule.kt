package com.dakarkaret.uklontest.app

import android.arch.persistence.room.Room
import android.content.Context
import com.dakarkaret.uklontest.repository.network.WebRepo
import com.dakarkaret.uklontest.repository.database.AppDatabase
import com.dakarkaret.uklontest.repository.network.WebService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: App) {
    @Provides
    fun provideApp(): App = app

    @Provides
    internal fun provideContext(app: App): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideWebRepo(): WebRepo = WebRepo()

    @Provides
    @Singleton
    fun provideWebService(): WebService = WebService()

    @Provides
    @Singleton
    fun providesDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "posts.db").build()

    @Provides
    fun providesPostDao(database: AppDatabase) = database.postDao()

    @Provides
    fun providesCommentDao(database: AppDatabase) = database.commentDao()
}